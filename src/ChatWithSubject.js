import React, {useState, useEffect} from 'react'
import app from './config'
import {Link, useParams} from "react-router-dom"
import SidePanel from "./SidePanel";
import {Card, CardActions, CardContent, Button, Typography, TextField} from '@material-ui/core/'
import ReactShadowScroll from 'react-shadow-scroll'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles({
    App: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        overflow: 'hidden'
    },
    formMessage: {
        bottom: 0,
        backgroundColor: 'white',
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'column',
        width: '100%'
    },
    chatBox: {
        margin: 'auto',
        marginBottom: '10px',
        marginTop: '10px',
        boxShadow: 'rgba(0, 0, 0, 0.2) 0px 1px 3px 0px, rgba(0, 0, 0, 0.14) 0px 1px 1px 0px, rgba(0, 0, 0, 0.12) 0px 2px 1px -1px',
        display: 'flex',
        flexDirection: 'column',
        width: '90%',
        backgroundColor: '#424242',
        maxHeight: '70vh',
        borderRadius: '2px',
    },
})

const ChatWithSubject = () => {
    const { userName } = useParams()
    const { key } = useParams()
    const { like } = useParams()
    const refTest = app.database().ref(`chat/${key}/message/`)
    const refLike = app.database().ref(`chat/${key}/like`)
    const [chatLike, setChatLike] = useState('')
    const [dataFirebase, setDataFirebase] = useState(null)
    const classes = useStyles()
    const [chatMessage, setChatMessage] = useState([])

    const uploadMessage = () => {
        app.database().ref(`/chat/${key}/message`)
            .push({
                message:chatMessage,
                name:userName
            })
    }

    const addLike = () => {
        setChatLike(chatLike + 1)
        app.database().ref(`/chat/${key}`)
            .update({
                like: chatLike,
            })
    }

    useEffect(() => {
            refTest.on("value", function (snapshot) {
                if(snapshot.val() === null) {
                    console.log("error")
                } else {
                    setDataFirebase(Object.entries(snapshot.val()))
                    console.log(dataFirebase)

                }
            })
        refLike.on("value", function (snapshot) {
            if(snapshot.val() === null) {
                console.log("error")
            } else {
                setChatLike(Object.values(snapshot.val()))
                console.log(chatLike)

            }
        })
    },[])
    return (
        <div>
            <SidePanel userName={userName}/>
            <h1>{key}</h1>
            <h3>{chatLike} personne(s) aime ce chat</h3>
            <Button onClick={addLike} variant="contained" color="primary">J'aime</Button>
            <ReactShadowScroll className={classes.chatBox}>
                { dataFirebase && dataFirebase.map(([key, data]) => (
                    <ul key={key}>
                        <li className={classes.liStyle}>{data.name} : {data.message}</li>
                    </ul>
                ))}
            </ReactShadowScroll>
            <form onSubmit={uploadMessage} className={classes.formMessage}>
                <TextField id="outlined-basic" label="Message" variant="outlined" onChange={(e) => setChatMessage(e.target.value)} />
            </form>
        </div>
    )
}

export default ChatWithSubject
