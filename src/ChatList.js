import React, {useEffect, useState} from "react";
import {Link, useParams} from "react-router-dom";
import {Card, CardActions, CardContent, ExpansionPanel, ExpansionPanelSummary, ExpansionPanelDetails, Button, Typography, TextField} from '@material-ui/core/'
import app from "./config"
import "./ChooseChatRoom.css"
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import SidePanel from "./SidePanel";

const ChatList = () => {
    const { userName } = useParams()
    const [dataFirebase, setDataFirebase] = useState(null)
    const ref = app.database().ref("chat/")

    useEffect(() => {
        ref.on("value", function (snapshot) {
            setDataFirebase(Object.entries(snapshot.val()))
            console.log(dataFirebase)
        })
    }, [])

    return (
        <div className="ChatList">
            <SidePanel userName={userName}/>
            { dataFirebase && dataFirebase.map(([key, data]) => (
                <ExpansionPanel key={key}>
                    <ExpansionPanelSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                    >
                    <Typography>{data.name}</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                    <Typography>{data.explanation}      like: {data.like}</Typography>
                        <Link to={`/chat/${userName}/${key}/${data.like}`}>
                    <Button color="primary" variant="contained">Entrer</Button>
                        </Link>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
            ))}
        </div>
    )
}

export default ChatList
