import React, {useState} from 'react';
import {Link} from "react-router-dom";
// Import Components of Material-Ui
import {Drawer, List, Divider, ListItem, ListItemText, AppBar, Toolbar, IconButton} from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';
import MenuIcon from "@material-ui/icons/Menu";


const useStyles = makeStyles({
    list: {
        width: 250,
    },
    listItem: {
        color: 'black',
        textDecoration: 'none'
    },
    buttonSidePanel: {
        margin: '30px 0'
    }
});

const SidePanel = ({userName}) => {
    const classes = useStyles();
    const [open, setOpen] = useState(false);
    const toggleDrawer = (open) => event => {
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }
        setOpen(open);
    };

    const sideList = () => (
        <div
            className={classes.list}
            role="presentation"
            onClick={toggleDrawer(false)}
            onKeyDown={toggleDrawer(false)}
        >
            <List>
                <Link to="/">
                    <ListItem button>
                        <ListItemText className={classes.listItem} primary="Accueil"/>
                    </ListItem>
                </Link>
                <Link to={`/chatlist/${userName}`}>
                    <ListItem button>
                        <ListItemText className={classes.listItem} primary="Choisir un chat"/>
                    </ListItem>
                </Link>
                <Link to={`/createchat/${userName}`}>
                    <ListItem button>
                        <ListItemText className={classes.listItem} primary="Créer un chat"/>
                    </ListItem>
                </Link>
                <Divider/>
            </List>
        </div>
    );

    return (
        <AppBar position="static">
            <Toolbar variant="dense">
                <IconButton onClick={() => setOpen(true)} edge="start" color="inherit" aria-label="menu">
                    <MenuIcon />
                </IconButton>
            </Toolbar>
            <Drawer open={open} onClose={toggleDrawer(false)}>
                {sideList()}
            </Drawer>
        </AppBar>
    );
};

export default SidePanel;