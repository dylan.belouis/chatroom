import React, {useEffect, useState} from "react";
import {Link, useParams} from "react-router-dom";
import {Card, CardActions, CardContent, Button, Typography, TextField} from '@material-ui/core/'
import app from "./config"
import "./ChooseChatRoom.css"

const ChooseChatRoom = () => {
    const [chatName, setChatName] = useState("")
    const [chatExplanation, setChatExplanation] = useState("")
    const [dataFirebase, setDataFirebase] = useState([])
    const ref = app.database().ref('chat/')

    const createNewChat = () => {
        app.database().ref( 'chat/' + chatName + '/').set({
            name: chatName,
            explanation: chatExplanation,
            like: "0"
        })
    }


    return (
        <div className="ChooseChatRoom">
            <form className="ChooseChatRoom_Form-create">
                <TextField variant="outlined" type="text" placeholder="Sujet du Chat" onChange={(e) => setChatName(e.target.value)}/>
                <TextField variant="outlined" type="text" placeholder="Explication du Chat" onChange={(e) => setChatExplanation(e.target.value)}/>
                <Button variant="contained" color="primary" onClick={createNewChat}>Valider la création</Button>
            </form>
        </div>
    )
}

export default ChooseChatRoom
