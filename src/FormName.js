import React, {useState} from "react";
import {Link} from "react-router-dom";
import {Card, CardActions, CardContent, Button, Typography, TextField} from '@material-ui/core/'
import { makeStyles } from '@material-ui/core/styles'
import fondForm from "./Everest.png";
import logoChat from "./123.png";
import './App.css'



const FormName = () => {
    const [userName, setUserName] = useState("")
    const [formOpen, setFormOpen] = useState(false)
    return (
            <div className="FormName">
                <img src={fondForm} alt="Fond" className="fond-SelectName" />
                    { formOpen ? (
                        <div className="form-Style">
                            <div className="display-Logo">
                                <img src={logoChat} alt="logo" className="logo_ChatApp" />
                            </div>
                            <Typography className="display-form_Element">Entre ton nom</Typography>
                            <TextField className="display-form_Element" variant="outlined" type="text" placeholder="nom" onChange={(e) => setUserName(e.target.value)} />
                            { userName ? (
                                <Link to={`/chatroom/${userName}`}>
                                    <Button className="display-form_Element" variant="contained"
                                            color="primary">Valider</Button>
                                </Link> ) : (<></>)
                            }
                        </div>) : (
                        <div className="form-Style">
                            <div className="display-Logo">
                                <img src={logoChat} alt="logo" className="logo_ChatApp" />
                            </div>
                            <Typography className="display-form_Element">Bienvenue sur notre Platforme de chat</Typography>
                            <Button className="display-form_Element" variant="contained" color="primary">En savoir plus</Button>
                            <Button className="display-form_Element" variant="contained" color="primary" onClick={() => setFormOpen(true)}>Commencer</Button>
                        </div>
                        )
                    }
            </div>
    );
}

export default FormName
