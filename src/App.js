import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import Chat from "./Chat"
import ChooseChatRoom from "./ChooseChatRoom"
import ChatList from "./ChatList"
import FormName from "./FormName"
import ChatWithSubject from "./ChatWithSubject"



const App = () => {
    return (
        <Router>
            <div className="App">
                <Switch>
                    <Route exact path="/">
                        <FormName />
                    </Route>
                    <Route exact path="/chatlist/:userName">
                        <ChatList />
                    </Route>
                    <Route exact path="/createchat/:userName">
                        <ChooseChatRoom />
                    </Route>
                    <Route path={`/chatroom/:userName`}>
                        <Chat />
                    </Route>
                    <Route path={`/chat/:userName/:key/:like`}>
                        <ChatWithSubject />
                    </Route>
                </Switch>
            </div>
        </Router>
    );
}

export default App
