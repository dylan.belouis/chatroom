import React, {useState, useEffect} from 'react'
import app from "./config"
import {Card, CardActions, CardContent, Button, Typography, TextField} from '@material-ui/core/'
import { makeStyles } from '@material-ui/core/styles'
import ReactShadowScroll from 'react-shadow-scroll'
import SidePanel from "./SidePanel"
import {
    useParams,
    Link
} from "react-router-dom";

const useStyles = makeStyles({
    App: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        overflow: 'hidden'
    },
    formMessage: {
        bottom: 0,
        backgroundColor: 'white',
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'column',
        width: '100%'
    },
    chatBox: {
        margin: 'auto',
        marginBottom: '10px',
        marginTop: '10px',
        boxShadow: 'rgba(0, 0, 0, 0.2) 0px 1px 3px 0px, rgba(0, 0, 0, 0.14) 0px 1px 1px 0px, rgba(0, 0, 0, 0.12) 0px 2px 1px -1px',
        display: 'flex',
        flexDirection: 'column',
        width: '90%',
        backgroundColor: '#424242',
        maxHeight: '70vh',
        borderRadius: '2px',
    },
})

const Chat = () => {
    const { userName } = useParams()
    const refTest = app.database().ref('message/')
    const [dataFirebase, setDataFirebase] = useState("")
    const [chatMessage, setChatMessage] = useState([])
    const classes = useStyles()

  const uploadMessage = () => {
      app.database().ref("message/")
          .push({
            message:chatMessage,
            name:userName
          })
  }

    useEffect(() => {
      refTest.on("value", function (snapshot) {
        setDataFirebase(Object.entries(snapshot.val()))
        console.log(dataFirebase)
      })
    },[])
  return (
    <div className={classes.App}>
        <SidePanel userName={userName}/>
        <h1>Chat Général</h1>
        <ReactShadowScroll className={classes.chatBox}>
                { dataFirebase && dataFirebase.map(([key, data]) => (
                <ul key={key}>
                    <li className={classes.liStyle}>{data.name} : {data.message}</li>
                </ul>
                ))}
        </ReactShadowScroll>
        <form onSubmit={uploadMessage} className={classes.formMessage}>
            <TextField id="outlined-basic" label="Message" variant="outlined" onChange={(e) => setChatMessage(e.target.value)} />
        </form>
    </div>
  );
}

export default Chat
