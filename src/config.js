import * as firebase from "firebase";
import "firebase/auth";

const app = firebase.initializeApp({
    apiKey: "AIzaSyCDF6BEdvs4E17Yhz3aoYBWaQ3c-wRkvCk",
    authDomain: "newprojectreact-3227c.firebaseapp.com",
    databaseURL: "https://newprojectreact-3227c.firebaseio.com",
    projectId: "newprojectreact-3227c",
    storageBucket: "newprojectreact-3227c.appspot.com",
    messagingSenderId: "867845442891",
    appId: "1:867845442891:web:1f81f66895cae36ffc2cce"
});

export default app;